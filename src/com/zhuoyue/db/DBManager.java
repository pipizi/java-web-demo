package com.zhuoyue.db;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Properties;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.zhuoyue.mapper.IMapper;

public class DBManager {
	
	//声明一个数据库连接池对象
	private static ComboPooledDataSource ds=new ComboPooledDataSource();
	//四大参数
	private static String driver="";
	private static String url="";
	private static String username="";
	private static String password="";
	static{
		try {
			InputStream in=DBManager.class.getClassLoader().getResourceAsStream("db.properties");
			Properties pro=new Properties();
			pro.load(in);
			//通过pro的key可以 获取数据了
			driver=pro.getProperty("driver");
			url=pro.getProperty("url");
			username=pro.getProperty("username");
			password=pro.getProperty("password");
			//初始化数据库连接池
			ds.setDriverClass(driver);
			ds.setJdbcUrl(url);
			ds.setUser(username);
			ds.setPassword(password);
			//初始化连接数需要在最大和最小连接数之间
			ds.setInitialPoolSize(10);
			//最大连接数在满足应用需要的情况下，参考默认值15
			ds.setMaxPoolSize(100);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private Connection getConn() throws Exception {
		Class.forName(driver);
		return ds.getConnection();
	}

	/*
	 * 我用来做增删改
	 */
	public void exectueUpdate(String sql, Object[] params) throws Exception {
		Connection conn = null;
		PreparedStatement stm = null;
		try {
			conn = this.getConn();
			//写入sql语句
			stm = conn.prepareStatement(sql);
			int len = params.length;
			//循环遍历输入参数
			for (int i = 0; i < len; i++) {
				stm.setObject(i + 1, params[i]);
			}
			// 执行sql
			stm.executeUpdate();
		} catch (Exception ex) {
			throw ex;
		} finally {
			stm.close();
			conn.close();
		}

	}
	
	/*
	 * 此处执行查询语句（第一个参数为sql语句，第二个参数为客户端传输来的参数，第三个参数为返回值类型，多态思想，继承IMapper接口）
	 */
	public List executeQuery(String sql,Object []params,IMapper mapper) 
		throws Exception{		
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs =null;
		try {
			conn = this.getConn();	
			stm = conn.prepareStatement(sql);
			int len = params.length;
			for (int i = 0; i < len; i++) {
				stm.setObject(i + 1, params[i]);
			}
			// 获取结果集
			rs= stm.executeQuery();
			List list=mapper.mapper(rs);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			rs.close();
			stm.close();
			conn.close();
		}
	}
	/*
	 * 聚合函数
	 */
	public int getCount(String sql,Object []params)
		throws Exception{
		Connection conn = null;
		PreparedStatement stm = null;
		ResultSet rs =null;
		try {
			conn=this.getConn();
			stm=conn.prepareStatement(sql);
			//设置参数
			int len = params.length;
			for (int i = 0; i < len; i++) {
				stm.setObject(i + 1, params[i]);
			}
			// 获取结果集
			rs= stm.executeQuery();
			int num=0;
			if(rs.next()){
				num=rs.getInt(1);
			}
			return num;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			rs.close();
			stm.close();
			conn.close();
		}	
	}

}



