package com.zhuoyue.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zhuoyue.dao.DeptDAO;
import com.zhuoyue.po.Dept;

/**
 * Servlet implementation class DoDeleteDeptAction
 */
@WebServlet("/dodeletedept")
public class DoDeleteDeptAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoDeleteDeptAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("utf-8");
		PrintWriter out=response.getWriter();
		
		String deptno=request.getParameter("deptno");
		
		Dept d=new Dept(new Integer(deptno));
		
		try {
			DeptDAO dao=new DeptDAO();
			dao.delete(d);
			
			response.sendRedirect("preshowdept");
		} catch (Exception e) {
			out.println("sorry!!!服务器正在升级维护中!!!");
			//发送一封邮件给开发人员
			
		}
		
		
	}

}
