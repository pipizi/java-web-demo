package com.zhuoyue.action;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zhuoyue.dao.EmpDAO;
import com.zhuoyue.po.Emp;

/**
 * Servlet implementation class DoSaveEmpAction
 */
@WebServlet("/dosaveemp")
public class DoSaveEmpAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoSaveEmpAction() {
        super();
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//how  
		// why what when
		// 练武不练功 到老一场空
		// 武(花拳绣腿)
		// 功
		String empno=request.getParameter("empno");
		String ename=request.getParameter("ename");
		String deptno=request.getParameter("deptno");
		
		Emp emp=new Emp();
		emp.setEmpno(new Integer(empno));
		emp.setEname(ename);
		emp.setDeptno(new Integer(deptno));
		
		try {
			EmpDAO dao=new EmpDAO();
			dao.save(emp);
			//重定向到查询
			response.sendRedirect("preshowemp");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
		
		
	}

}



