package com.zhuoyue.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zhuoyue.dao.DeptDAO;
import com.zhuoyue.po.Dept;
import com.zhuoyue.util.PageUtil;

/**
 * Servlet implementation class PreShowDeptAction
 */
@WebServlet("/preshowdept")
public class PreShowDeptAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DeptDAO dao=new DeptDAO();
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PreShowDeptAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
//		System.out.println(request.getRemoteAddr());
//		System.out.println(request.getRemoteHost());
//		System.out.println(request.getRemoteUser());
//		System.out.println(request.getRemotePort());
		
		int pagenow=1;//default
		int pagesize=3;//default
		int count=0;
				
		try {
			count=dao.getCount();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		
		String strpagenow=request.getParameter("pagenow");
		if(strpagenow!=null){//客户端传递来了
			pagenow=new Integer(strpagenow);
		}
		String strpagesize=request.getParameter("pagesize");
		if(strpagesize!=null){
			pagesize=new Integer(strpagesize);
		}
		
		int totalpage=PageUtil.getTotalPage(count, pagesize);
		//
		request.setAttribute("pagenow",pagenow);
		request.setAttribute("count", count);
		request.setAttribute("totalpage", totalpage);
		request.setAttribute("pagesize", pagesize);
		
		
		
		
		
		try {
			
			//List<Dept> list=dao.findAll();
			List<Dept> list=dao.findByPage(pagenow,pagesize);
			request.setAttribute("list", list);
			request.getRequestDispatcher("showdept.jsp").forward(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

}
