package com.zhuoyue.action;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zhuoyue.dao.DeptDAO;
import com.zhuoyue.dao.EmpDAO;
import com.zhuoyue.po.Dept;
import com.zhuoyue.po.Emp;

/**
 * Servlet implementation class DoMergeDeptAction
 */
@WebServlet("/domergeemp")
public class DoMergeEmpAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoMergeEmpAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String empno=request.getParameter("empno");
		String ename=request.getParameter("ename");
		String deptno=request.getParameter("deptno");
		
		Emp e=new Emp();
		e.setEmpno(new Integer(empno));
		e.setEname(ename);
		e.setDeptno(new Integer(deptno));
		
		try {
			EmpDAO dao=new EmpDAO();
			dao.merge(e);
			response.sendRedirect("preshowemp");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
