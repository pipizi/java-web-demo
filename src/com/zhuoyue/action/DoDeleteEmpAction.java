package com.zhuoyue.action;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zhuoyue.dao.DeptDAO;
import com.zhuoyue.dao.EmpDAO;
import com.zhuoyue.po.Dept;
import com.zhuoyue.po.Emp;

/**
 * Servlet implementation class DoDeleteDeptAction
 */
@WebServlet("/dodeleteemp")
public class DoDeleteEmpAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoDeleteEmpAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("utf-8");
		PrintWriter out=response.getWriter();
		String empno=request.getParameter("empno");
		Emp e = new Emp();
		e.setEmpno(new Integer(empno));
		try {
			EmpDAO dao=new EmpDAO();
			dao.delete(e);
			response.sendRedirect("preshowemp");
		} catch (Exception e1) {
			out.println("sorry!!!服务器正在升级维护中!!!");
			//发送一封邮件给开发人员
		}
		
		
	}

}
