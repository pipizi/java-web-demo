package com.zhuoyue.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zhuoyue.dao.DeptDAO;
import com.zhuoyue.po.Dept;

/**
 * Servlet implementation class PreSaveEmpAction
 */
@WebServlet("/presaveemp")
public class PreSaveEmpAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PreSaveEmpAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			DeptDAO dao=new DeptDAO();
			List<Dept> list=dao.findAll();//
			request.setAttribute("list", list);
			request.getRequestDispatcher("saveemp.jsp").forward(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

}
