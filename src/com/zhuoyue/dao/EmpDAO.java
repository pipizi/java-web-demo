package com.zhuoyue.dao;

import java.util.List;

import com.zhuoyue.db.DBManager;
import com.zhuoyue.mapper.DeptMapper;
import com.zhuoyue.mapper.EmpMapper;
import com.zhuoyue.po.Dept;
import com.zhuoyue.po.Emp;
import com.zhuoyue.util.PageUtil;

public class EmpDAO {
	
	private DBManager db=new DBManager();
	
	public int getCount()
		throws Exception{
		String sql=" select count(*) from emp ";
		Object []params={};
		return db.getCount(sql, params);
	}
	
	public void save(Emp emp)
		throws Exception{
		String sql=" INSERT INTO emp (empno,ename,deptno) VALUES (?,?,?) ";
		Object []params={emp.getEmpno(),emp.getEname(),emp.getDeptno()};
		db.exectueUpdate(sql, params);
	}
	public void delete(Emp emp)
		throws Exception{
		String sql=" delete from emp where empno=? ";
		Object []params={emp.getEmpno()};
		db.exectueUpdate(sql, params);
		
	}
	public void merge(Emp emp)
		throws Exception{
		String sql=" update emp set deptno=?,ename=? where empno=? ";
		Object []params={emp.getDeptno(),emp.getEname(),emp.getEmpno()};
		db.exectueUpdate(sql, params);
	}
	/*
	 * 从今天开始，此方法基本上没有用了...
	 */
	public List<Emp> findAll()
			throws Exception{
			String sql="select *from emp ";
			Object []params={};
			return db.executeQuery(sql, params,new EmpMapper());
	}
	
	public List<Emp> findByPage(int pagenow,int pagesize)
			throws Exception{
			String sql="select *from emp limit ?,?";
			//开始条数
			int start=PageUtil.getStart(pagenow, pagesize);
			Object []params={start,pagesize};
			
			return db.executeQuery(sql, params,new EmpMapper());
	}
		
	
	public Emp findById(int empno)
		throws Exception{
		String sql=" select *from emp where empno=? ";
		Object []params={empno};
		List<Emp> list=db.executeQuery(sql, params,new EmpMapper());//最多查询到一个  1
																	  //查不着	  0
		if(list.size()>0){
			return list.get(0);
		}
		return null;//上层调用的时候记得判定...
	}
	
}
