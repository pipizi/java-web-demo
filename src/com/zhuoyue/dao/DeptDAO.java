package com.zhuoyue.dao;

import java.util.List;

import com.zhuoyue.db.DBManager;
import com.zhuoyue.mapper.DeptMapper;
import com.zhuoyue.po.Dept;
import com.zhuoyue.util.PageUtil;

public class DeptDAO {
	
	private DBManager db=new DBManager();
	
	public int getCount()
		throws Exception{
		String sql=" select count(*) from dept ";
		Object []params={};
		return db.getCount(sql, params);
		
	}
	
	public void save(Dept dept)
		throws Exception{
		String sql=" insert into dept values (?,?,?) ";
		Object []params={dept.getDeptno(),dept.getDname(),dept.getLoc()};
		db.exectueUpdate(sql, params);
	}
	public void delete(Dept dept)
		throws Exception{
		String sql=" delete from dept where deptno=? ";
		Object []params={dept.getDeptno()};
		db.exectueUpdate(sql, params);
		
	}
	public void merge(Dept dept)
		throws Exception{
		String sql=" update dept set dname=?,loc=? where deptno=? ";
		Object []params={dept.getDname(),dept.getLoc(),dept.getDeptno()};
		db.exectueUpdate(sql, params);
	}
	/*
	 * 从今天开始，此方法基本上没有用了...
	 * 
	 */
	public List<Dept> findAll()
			throws Exception{
			String sql="select *from dept order by deptno";
			Object []params={};
			return db.executeQuery(sql, params,new DeptMapper());
	}
	
	public List<Dept> findByPage(int pagenow,int pagesize)
			throws Exception{
			String sql="select *from dept limit ?,?";
			//开始条数
			int start=PageUtil.getStart(pagenow, pagesize);
			Object []params={start,pagesize};
			
			return db.executeQuery(sql, params,new DeptMapper());
	}
		
	
	public Dept findById(int deptno)
		throws Exception{
		String sql=" select *from dept  where deptno=? ";
		Object []params={deptno};
		List<Dept> list=db.executeQuery(sql, params,new DeptMapper());//最多查询到一个  1
																	  //查不着	  0
		if(list.size()>0){
			return list.get(0);
		}
		return null;//上层调用的时候记得判定...
	}
}

