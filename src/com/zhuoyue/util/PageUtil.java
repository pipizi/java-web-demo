package com.zhuoyue.util;

public class PageUtil {
	
	public static int getTotalPage(int count,int pagesize){
		return (count-1)/pagesize+1;
	}
	
	public static int getStart(int pagenow,int pagesize){
		
		return (pagenow-1)*pagesize;
		
	}
	
	public static void main(String[] args) {
		//18 3 6
		int totals=PageUtil.getTotalPage(78, 3);
		System.out.println(totals);
	}
	
}
