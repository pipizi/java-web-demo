package com.zhuoyue.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.zhuoyue.po.Emp;

public class EmpMapper implements IMapper {
	
	@Override
	public List mapper(ResultSet rs)
			throws Exception{
		List<Emp> list = new ArrayList<>();
		while (rs.next()) {// 结果集封装成对象
			// 获取内容的时候
			// 根据行中的列的下标
			int empno = rs.getInt(1);
			String ename = rs.getString(2);
			String job = rs.getString(3);
			int deptno=rs.getInt(8);
			//.......
			Emp e = new Emp();
			e.setEmpno(empno);
			e.setEname(ename);
			e.setJob(job);
			e.setDeptno(deptno);
			list.add(e);
		}
		return list;
		
	}
	
	
}
