package com.zhuoyue.mapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.zhuoyue.po.Dept;

public class DeptMapper implements IMapper {
	
	@Override
	public List mapper(ResultSet rs)throws Exception{
		List<Dept> list = new ArrayList<>();
		while (rs.next()) {// 结果集封装成对象
			// 获取内容的时候根据行中的列的下标
			int deptno = rs.getInt(1);
			String dname = rs.getString(2);
			String loc = rs.getString(3);
			Dept d = new Dept();
			d.setDeptno(deptno);
			d.setDname(dname);
			d.setLoc(loc);
			list.add(d);
		}
		return list;
		
	}
	

}
