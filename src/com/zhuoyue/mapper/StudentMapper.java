package com.zhuoyue.mapper;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import com.zhuoyue.po.Student;

public class StudentMapper implements IMapper {
	
	@Override
	public List mapper(ResultSet rs) throws Exception {
		List<Student> list = new ArrayList<>();
		while (rs.next()) {// 结果集封装成对象
			// 获取内容的时候
			// 根据行中的列的下标
			int stuid=rs.getInt(1);
			String stuname=rs.getString(2);
			Student stu=new Student();
			stu.setStuid(stuid);
			stu.setStuname(stuname);
			list.add(stu);
		}
		return list;
	}
	
	
}
