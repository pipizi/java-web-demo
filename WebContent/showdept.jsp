<%@page import="java.util.List"%>
<%@page import="com.zhuoyue.po.Dept"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css">
	
	td{
	
		font-size:12px;
	
	}
	
</style>
<script>
	function dojump(){
		
		var value=document.getElementById("num").value;
		//alert(value);
		//所谓的跳转就是重新向服务器发送一次查询请求
		//带上当前页
		window.location.href="preshowdept?pagenow="+value;
	}
</script>
</head>
<body>
	<h1 align="center">Dept Message</h1>
	<center>
		<a href="savedept.jsp">save dept</a>&nbsp;
		<a href="preshowemp">show emp</a>
	</center>
	<table border="1" bordercolor="gray" cellspacing="0" cellpadding="0" align="center" width="700">
		<tr>
			<th>DeptNo</th>
			<th>Dname</th>
			<th>Loc</th>
			<th>Operator</th>
		</tr>
		<%
		List<Dept> list=(List<Dept>)request.getAttribute("list");
		int size=list.size();
		for(int i=0;i<size;i++){
			Dept d=list.get(i);
		%>
		<tr bgcolor="<%=i%2==0?"":"#7FFFD4"%>">
			<td><%=d.getDeptno() %></td>
			<td><%=d.getDname() %></td>
			<td><%=d.getLoc() %></td>
			<td>
			<a href="dodeletedept?deptno=<%=d.getDeptno() %>">delete</a>
			
			<a href="premergerdept?deptno=<%=d.getDeptno() %>">merge</a>
			
			</td>
		</tr>
		<%} %>
		<tr>
			<td colspan="4" >
				<%
					int pagenow=new Integer(request.getAttribute("pagenow").toString());
					int count=new Integer(request.getAttribute("count").toString());
					int totalpage=new Integer(request.getAttribute("totalpage").toString());
					int pagesize=new Integer(request.getAttribute("pagesize").toString());
				%>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<%
					if(pagenow<=1){
				%>
						首页 上一页
				<%	
					}else{
				%>
				<a href="preshowdept?pagenow=1">首页</a> 
				<a href="preshowdept?pagenow=<%=pagenow-1%>">上一页</a>
				<%}%> 
				<%
					if(pagenow<totalpage){
				%>
				<a href="preshowdept?pagenow=<%=pagenow+1%>">下一页 </a>
				<a href="preshowdept?pagenow=<%=totalpage %>">尾页 </a>
				<%
					}else{
				%>
					              下一页 尾页
				<%} %>
				
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
				当前<%=pagenow %>/<%=totalpage %>页
				
				每页:<%=pagesize %>条
				
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
				跳转:<input size="5" id="num" /><input type="button" value="jump" onclick="dojump()" />
				
			</td>
		</tr>
	</table>
	
</body>
</html>